# soal-shift-sisop-modul-1-I07-2022
## **Laporan Resmi dan Penjelasan Soal Shift Modul 1**
### **Kelompok I-07**
**Anggota :**
- Selomita Zhafirah 5025201120
- Amelia Mumtazah Karimah 5025201128
- Mohammed Fachry Dwi Handoko 5025201159

## **Daftar Isi Lapres**
- [Soal-1](#soal1)
- [Soal-2](#soal2)
- [Soal-3](soal3)


# Soal 1
Setiap variabel ```users``` menyimpan semua akun (username & password) yang sudah terdaftar
```bash
location="users/user.txt"
```
Input dari users akan disimpan ke variabel ```username``` 
```bash
echo "username: "
read uname; 

bool=1
while [ $bool -eq 1 ]
do 
```
Setelah berhasil menginput username step selanjutnya adalah membuat password. Adapun kriteria dalam membuat password, sebagai berikut;
1. Minimal menggunakan 8 karakter.
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil.
3. Menggunakan alphanumeric.
4. Password tidak diperkenankan sama dengan username.
Selama password yang diinput belum sesuai kriteria diatas, maka user akan terus diminta untuk menginput password sesuai kriteria.
```bash
echo "password: "
read -s pass;
if [ ${#pass} -lt 8 ];
then
echo "please add more character"
elif ! [[ "$pass" =~ [A-Z] ]] || ! [[ "$pass" =~ [a-z] ]];
then 
echo "please use at least 1 uppercase and 1 lowercase"
elif [[ "$pass" =~ [^a-zA-Z0-9] ]];
then
echo "please use alphanumeric"
elif [[ "$uname" == "$pass" ]];
then
echo "username cant same with password"
else 
bool=0
fi 
done
```
Setelah menginput ```username``` dan ```password``` berhasil, maka username dan password akan langsung terdata ke ```users/user.txt```
```bash
echo 'Username:'"${username}" >> ./users/user.txt
echo 'Password:'"${password}" >> ./users/user.txt
```
Tampilkan juga statement bahwa register telah berhasil dan tercatat di ```log.txt```
```bash
time=$(date +"%m%d%y %H:%M:%S")
if grep -qF "$uname" $location;
then
echo $time "REGISTER: ERROR User already exists" >> log.txt 
else 
echo $time "REGISTER: INFO User $uname registered successfully" >> log.txt
echo "username:" $unamSe "password:" $pass >> $location
fi 
```
Sistem login berada di ```main.sh```. Jika username yang diinput oleh user tidak terdaftar di ```user.txt``` maka program akan meminta terus hingga pengguna menginputkan username yang telah terdaftar di ```user.txt```. Setelah itu dilakukan pengecekan password dan hasilnya dicatat ke ```log.txt```
```bash
echo "Login Attempt"
echo -n "username: "
read uname;
echo -n "password: "
read pass;

location="users/user.txt"

if grep -qF "$pass" $location
then
echo "LOGIN: INFO User $uname logged in" >> log.txt
```
Setelah users berhasil login, pengguna dapat mengetikkan dua command ```dl N``` dan ```att```  
untuk **function** command ```dl N``` mendownload **N** gambar dari ```https://loremflickr.com/320/240``` dan dimasukkan kedalam sebuah file lalu di zip. Apabila file zip sudah ada, maka isinya akan ditambahkan. Langkah terakhir diberi ```password``` sesuai user yang sedang login
```bash
cho "choose dl/att"
read choose;

if [ "$choose" == "dl" ];
then
echo "amount of pictures"
read n;

folder=$(date +%Y-%m-%d)_$uname

if [[ ! -f "$folder.zip" ]]
then
mkdir $folder
function_download
else
function_unzip
fi
# count=1

elif [ "$choose" == "att" ]
then
awk ' BEGIN { print "Counting!!" }
/logged/{login++} /Failed/{fail++}
END { print "Success:", login, "Failed:", fail }' log.txt
fi
else
echo "LOGIN: ERROR Failed login attempt on User $uname" >> log.txt
fi
```
# Soal 2
## **Rata-rata request Perjam**
Pisahkan field dan mulai dari baris kedua. Menggunakan ```sum += $3; arr[$3]++``` 
```bash
cat > soal2_forensic.dapos.sh 
awk -F: '{ NR>1; sum += $3; arr[$3]++ }`
```
Banyak request ```$3``` di total dan diabgi dengan banyaknya baris atau ```jam``` dan didapat rata - ratanya
```bash
 for(i in arr){ req++; sum+=arr[i];}
```
Digabung dengan pipe dan catat di ```ratarata.txt```
```bash
cat > soal2_forensic.dapos.sh
awk -F: '{ NR>1; sum += $3; arr[$3]++ } END { for(i in arr){ req++; sum+=arr[i];} print("Rata-rata serangan adalah sebanyak:\n", sum/req, " per jam") }'  log_website_daffainfo.log > ratarata.txt
```
## **IP Terbanyak Request & Jumlah Request**
Pisahkan field dan mulai dari baris kedua. Menggunakan ```{if ($1>!+max) max=$1}``` untuk menampilkan IP address yang merequest
```bash
awk -F: 'BEGIN{ NR>1; max=0'
```
Digabung dengan pipe dan catat di ```result.txt```
```bash
awk -F: 'BEGIN{ NR>1; max=0 END{print "IP yang paling banyak mengakses server adalah: \n", $1}' log_website_daffainfo.log > result.txt
```
## **Jumlah Request yang Menggunakan Curl**
Untuk melakukan request yang menggunakan user-agent curl, pada log ada tulisan curlnya. Jadi cukup menghitung baris yang terdapat ```'curl'```. Hasilnya bisa dicatat di ```result2.txt```
```bash
awk -F: 'BEGIN{$6 = "curl"; max=0;} { if (max = $1 + 1){ max = $3 } } END{ print "Ada :", max, "\n users yang menggunakan curl sebagai username" }' log_website_daffainfo.log > result2.txt
```
## **Daftar Request IP pada Pukul 2 Pagi**
```bash
awk -F '22/Jan/2022:02:' '{print $2}' log_website_daffainfo.log > result3.txt
forensic_log_website_daffainfo_log/result.txt
```
Berikut kode program lengkapnya :

```bash
cat > soal2_forensic.dapos.sh
echo 'SISOP || TC 2020'
chmod u+x soal2_forensic.dapos.sh

awk -F: '{ NR>1; sum += $3; arr[$3]++ } END { for(i in arr){ req++; sum+=arr[i];} print("Rata-rata serangan adalah sebanyak:\n", sum/req, " per jam") }'  log_website_daffainfo.log > ratarata.txt

awk -F: 'BEGIN{ NR>1; max=0 END{print "IP yang paling banyak mengakses server adalah: \n", $1}' log_website_daffainfo.log > result.txt

awk -F: 'BEGIN{$6 = "curl"; max=0;} { if (max = $1 + 1){ max = $3 } } END{ print "Ada :", max, "\n users yang menggunakan curl sebagai username" }' log_website_daffainfo.log > result2.txt

awk -F '22/Jan/2022:02:' '{print $2}' log_website_daffainfo.log > result3.txt
```
# Soal 3
